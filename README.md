**Instuction to use:**<pre>
1] Clone the application from git repository:
        *git clone {git repository} {example AppDirectory}*
   **Example:**
           *git clone http://&lt;username&gt;@gitlab.cse.iitb.ac.in/IITBombayXCustomRegForm/IITBomabayXCustomRegForm.git*
   <br>**For IITBombayX:**
        *git checkout IITBomabayX*<br>
   <br>**For AfghanX:**
        *git checkout Afghanx*<br>
      
2] Change ownership of directory AppDirectory as provided above to edxapp:
   *sudo chown -R edxapp:edxapp AppDirectory*

3] **To install the application:**<pre>
    a] Check the Internet connection.<br>
    b] Make sure current working directory is /home/edx.
            *pwd*
        If not, Change to home directory.
            *cd*<br>
    c] Execute the command on shell:
            *sudo -H -u edxapp bash*<br>
    d] Activate edx platform virtual environment
            *source /edx/app/edxapp/edxapp_env*<br>            
    e] Install the application:
            *pip install -e AppDirectory/*<br>
    f] Exit from shell:
            *exit*</pre>

4] Add "custom_reg_form" to the "ADDL_INSTALLED_APPS" array in `lms.env.json` (you may have to create it if it doesn't exist.)
**Note:** lms.env.json file are located one level above the edx-platform directory. 
  **Example:**:
    *"ADDL_INSTALLED_APPS": ["custom_reg_form"],*

5] Set "REGISTRATION_EXTENSION_FORM" to "custom_reg_form.forms.ExtraInfoForm" in `lms.env.json`. (you may have to create it if it doesn't exist.)
**Note:** lms.env.json file are located one level above the edx-platform directory.
  **Example:**:
    *"REGISTRATION_EXTENSION_FORM": "custom_reg_form.forms.ExtraInfoForm",*

6] Set the ENABLE_COMBINED_LOGIN_REGISTRATION feature flag to True in cms.env.json. (you may have to create it if it doesn't exist.)
**Note:** cms.env.json file are located one level above the edx-platform directory.
  **Example:**:
    *"ENABLE_COMBINED_LOGIN_REGISTRATION": true,*

7] **To run migrations:**<pre>
    a] Check the Internet connection.<br>
    b] Make sure current working directory is /home/edx.
            *pwd*
        If not, Change to home directory.
            *cd*<br>
    c] Execute the command on shell:
            *sudo -H -u edxapp bash*<br>
    d] Activate edx platform virtual environment:
            *source /edx/app/edxapp/edxapp_env*<br>    
    e] Change to edx-platform directory:
            *cd /edx/app/edxapp/edx-platform/*<br>  
    f] Run Migrations:
            *./manage.py lms migrate custom_reg_form  --settings=aws*<br>
    g] Exit from shell:
            *exit*</pre>

8] Start/restart the LMS.
        *sudo /edx/bin/supervisorctl restart lms*</pre>
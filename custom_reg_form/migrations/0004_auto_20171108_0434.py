# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_reg_form', '0003_auto_20171030_0205'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='mooccity',
            table=None,
        ),
        migrations.AlterModelTable(
            name='moocperson',
            table=None,
        ),
        migrations.AlterModelTable(
            name='moocstate',
            table=None,
        ),
    ]

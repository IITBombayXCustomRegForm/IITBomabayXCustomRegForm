# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_reg_form', '0004_auto_20171108_0434'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moocperson',
            name='aadhar_id',
            field=models.BigIntegerField(default=None, max_length=12, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='moocperson',
            name='pincode',
            field=models.IntegerField(max_length=6, error_messages={b'required': b'pincode is required .'}),
        ),
    ]

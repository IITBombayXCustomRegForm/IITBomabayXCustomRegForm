# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MoocCity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='MoocPerson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pincode', models.IntegerField(max_length=6, error_messages={b'required': b'pincode is required .'})),
                ('aadhar_id', models.BigIntegerField(default=None, max_length=12, null=True, blank=True)),
                ('city', models.ForeignKey(default=None, to='custom_reg_form.MoocCity')),
            ],
        ),
        migrations.CreateModel(
            name='MoocState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, unique=True, max_length=255, db_index=True)),
            ],
        ),
        migrations.AddField(
            model_name='moocperson',
            name='state',
            field=models.ForeignKey(default=None, choices=[(b'1', b'Andaman and Nicobar islands'), (b'2', b'Andhra Pradesh'), (b'3', b'Arunachal Pradesh'), (b'4', b'Assam'), (b'5', b'Bihar'), (b'6', b'Chandigarh'), (b'7', b'Chattisgarh'), (b'8', b'Dadra and Nagar Haveli'), (b'9', b'Daman and Diu'), (b'10', b'Delhi'), (b'11', b'Goa'), (b'12', b'Gujarat'), (b'13', b'Haryana'), (b'14', b'Himachal Pradesh'), (b'15', b'Jammu and Kashmir'), (b'16', b'Jharkhand'), (b'17', b'Karnataka'), (b'18', b'Kerala'), (b'19', b'Lakshadweep'), (b'20', b'Madhya Pradesh'), (b'21', b'Maharashtra'), (b'22', b'Manipur'), (b'23', b'Meghalaya'), (b'24', b'Mizoram'), (b'25', b'Nagaland'), (b'26', b'Orissa'), (b'27', b'Pondicherry'), (b'28', b'Punjab'), (b'29', b'Rajasthan'), (b'30', b'Sikkim'), (b'31', b'Tamil Nadu'), (b'36', b'Telangana'), (b'32', b'Tripura'), (b'34', b'Uttar Pradesh'), (b'33', b'Uttarakhand'), (b'35', b'West Bengal')], to='custom_reg_form.MoocState'),
        ),
        migrations.AddField(
            model_name='moocperson',
            name='user',
            field=models.OneToOneField(default=None, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mooccity',
            name='state',
            field=models.ForeignKey(to='custom_reg_form.MoocState'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_reg_form', '0002_auto_20171030_0202'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='mooccity',
            table='student_mooc_city',
        ),
        migrations.AlterModelTable(
            name='moocperson',
            table='student_mooc_person',
        ),
        migrations.AlterModelTable(
            name='moocstate',
            table='student_mooc_state',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_reg_form', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moocperson',
            name='aadhar_id',
            field=models.CharField(default=None, max_length=12, null=True, verbose_name=b'aadhar', blank=True),
        ),
        migrations.AlterField(
            model_name='moocperson',
            name='pincode',
            field=models.CharField(max_length=6, verbose_name=b'pincode', error_messages={b'required': b'pincode is required .'}),
        ),
        migrations.AlterField(
            model_name='moocperson',
            name='state',
            field=models.ForeignKey(default=None, to='custom_reg_form.MoocState'),
        ),
        migrations.AlterModelTable(
            name='mooccity',
            table='"student_mooc_city"',
        ),
        migrations.AlterModelTable(
            name='moocperson',
            table='"student_mooc_person"',
        ),
        migrations.AlterModelTable(
            name='moocstate',
            table='"student_mooc_state"',
        ),
    ]

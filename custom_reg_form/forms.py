"""
Django Form and Utility functions for validating forms
"""

from .models import MoocPerson,MoocCity,MoocState
from django import forms
from django.forms import ModelForm,ValidationError,CharField,ChoiceField
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.validators import validate_integer
import re
import logging
log=logging.getLogger(__name__)


class ExtraInfoForm(ModelForm):
    """
    Form that wraps around model MoocPerson 
    """
    city = CharField(label="City",error_messages={'required': 'Please enter your City.'},initial="Mumbai")
    states= tuple([('', '--')]+[(pref.id,pref.name) for pref in MoocState.objects.all()])
    state =ChoiceField(choices=states, label="State",error_messages={'required': 'Please enter your State.'})
    pincode = CharField(label="Pincode",error_messages={'required': 'Please enter your Pincode.'},initial="123456",max_length="6")
    aadhar_id = CharField(label="Aadhar ID (UIDAI) ",initial="123456789012",max_length="12",required=False)
    def __init__(self, *args, **kwargs):
        super(ExtraInfoForm, self).__init__(*args, **kwargs)

 
    class Meta:
        model = MoocPerson
        fields = ('state','city','pincode','aadhar_id')

    def clean_pincode(self):
        """Enforce pincode validations"""
        pincode=self.cleaned_data['pincode']
        try:
            validate_integer(pincode)
        except:
            raise ValidationError(_("Pincode must be integer and should not start with 10, 01 or 00."))
        log.info(len(pincode))
        if pincode and len(pincode)<6:
            raise ValidationError(_("Pincode must have at least 6 characters."))
        if pincode[0]=='0' or pincode[:2]=='10':
            raise ValidationError(_("Pincode must be integer and should not start with 10, 01 or 00."))
        return pincode

    def clean_aadhar_id(self):
        """Enforce aadhar id policies"""
        aadhar_id=self.cleaned_data['aadhar_id']
        if aadhar_id:
            try:
               validate_integer(aadhar_id)
            except:
               raise ValidationError(_("Aadhar ID must be integer."))
        if aadhar_id and len(aadhar_id)<12:
               raise ValidationError(_("Aadhar ID must be an integer of at least 12 digits."))
        return aadhar_id 

    def clean_city(self):
        """Enforce city validation and return MoocCity object """
        city=self.cleaned_data['city']
        if len(city) < 3:
            raise ValidationError("City must have at least 3 characters.")
        if not re.match('^[a-zA-Z0-9]+[ ,a-zA-Z0-9]*[a-zA-Z]+$',city) or city.isdigit():
            raise ValidationError("City must contain only characters with no special character.")

        city_name=self.cleaned_data['city']
        city_name = ' '.join(city_name.split())
        # Each Word of city should start with Upper case
        city_name = re.sub("(^|\s)(\S)", lambda m:m.group(1) + m.group(2).upper(), city_name)
        city,created = MoocCity.objects.get_or_create(name=city_name,state_id=self.cleaned_data['state'].id)
        return city

    def clean_state(self):
        """Return MoocState object """
        state,created= MoocState.objects.get_or_create(id=self.cleaned_data['state'])
        return state

    def save(self, commit=True):
        instance = super(ExtraInfoForm,self).save(commit=commit)
        instance.city=self.cleaned_data['city']
        instance.state=self.cleaned_data['state'] 
        instance.pincode=self.cleaned_data['pincode']
        if self.cleaned_data['aadhar_id'] =='':
            instance.aadhar_id=None
        else: 
            instance.aadhar_id=self.cleaned_data['aadhar_id']
        if commit:
            instance.save()
        return instance 
    

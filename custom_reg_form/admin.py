from django.contrib import admin
from .models import MoocState,MoocCity

admin.site.register(MoocState)
admin.site.register(MoocCity)

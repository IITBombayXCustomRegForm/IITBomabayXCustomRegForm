""" Models for the person entity, state, and city """

from django.conf import settings
from django.db import models
import logging
log=logging.getLogger(__name__)

# Backwards compatible settings.AUTH_USER_MODEL
USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class MoocState(models.Model):
      """
      This model contains master state field.
      """ 

      name=models.CharField(max_length=255,unique=True,db_index=True,null=False,default=None)

      def __unicode__(self):

          return self.name

class MoocCity(models.Model):

      """
      This model contains mapped city to state field.
      """ 
      name= models.CharField(max_length=255,null=False,default=None)

      state = models.ForeignKey(MoocState)

      def __unicode__(self):

          return self.name

class MoocPerson(models.Model):
      """
      This model contains four extra fields that will be saved when a user registers.
      The form that wraps this model is in the forms.py file.
      """
      
      user = models.OneToOneField(USER_MODEL,null=False,default=None)

      state = models.ForeignKey(MoocState,default=None)

      city= models.ForeignKey(MoocCity,default=None)

      pincode=models.IntegerField(max_length=6, error_messages={'required': 'pincode is required .'})

      aadhar_id = models.BigIntegerField(max_length=12,default=None,blank=True,null=True)
      
      def __unicode__(self):
          
          return self.user
      

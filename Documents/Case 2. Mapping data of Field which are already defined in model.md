# 2. Mapping data of Field which are already defined in model
<pre>
	<i>e.g.</i> first_name, last_name
        ◦ <b>"first_name"</b> is field with label "First Name" on registration page is not map anywhere in database while “auth_user" table has "first_name" column.
        ◦ <b>"last_name"</b> is field with label "Last Name" on registration page is not map anywhere while auth_user" table has "last_name" column.
</pre>


***Make following changes in ```do_create_account``` function of “edx-platform/common/djangoapps/student/helpers.py” file, to save data of "first_name" and  “last_name” fields in ```auth_user``` table of ```edxapp``` database in columns "first_name" and  “last_name” respectively***
```
proposed_first_name = form.cleaned_data["first_name"] 
proposed_last_name = form.cleaned_data["last_name"]
user = User(
        username=proposed_username,
        first_name=proposed_first_name,
        last_name=proposed_last_name,
        email=form.cleaned_data["email"],
        is_active=False
)
```
***Let's now add ```first_name```  and ```last_name``` fields which is currently not available. Open the lms environment setting file.***

```shell
sudo vi /edx/app/edxapp/lms.env.json
```

```
    "REGISTRATION_EXTRA_FIELDS": {
        "first_name": "required",
        "last_name": "required",
        "job_title": "hidden",
        "confirm_email": "required",
        "city": "hidden",
        "state": "required",
        "country": "hidden",
        "gender": "optional",
        "goals": "optional",
        "honor_code": "required",
        "level_of_education": "optional",
        "mailing_address": "optional",
        "terms_of_service": "optional",
        "year_of_birth": "optional",
        "company": "optional",
        "title": "required",
        "profession": "required",
        "specialty": "required"
    },
```
Now save the file.

**Restart lms service by running the following command**

```shell
sudo /edx/bin/supervisorctl restart lms
```


## Test:

1. Now browse the OpenEdx installation in your browser (```http://ipaddress```) 
2. To see the changes in registration form of Open edX Ironwood.master, Click on "Register" Button.
``` first_name ``` and  ```last_name```  fields are now available in mandatory section registration page.
3. Check captured data from this field is stored in ```auth_user``` table of ```edxapp``` database of MySQL server.
```shell
mysql -u root -p
Enter password: 
```
In MySQL, to confirm 'state' column field in ```auth_userprofile``` table of edxapp database
```
use edxapp
select * from auth_user order by id desc limit 2;
exit
```